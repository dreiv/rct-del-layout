import { Box, Button, Card, CardActions, CardContent, CardHeader, Typography } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import SaveIcon from '@material-ui/icons/Save';
import React from 'react';

import useStyles from './ItemDetails.styles';

const ItemDetails = ({
	itemName,
	item,
	onDeleteItem,
	onResetItem,
	onSaveItem,
	content
}) => {
	const classes = useStyles()

	return (
		<Card>
			<CardHeader title={itemName + ' Details'} />

			<CardContent>
				{!item
					? (
						<Box
							display="flex"
							justifyContent="center"
							alignItems="center"
							className={classes.empty}
						>
							<Typography>Select or add a {itemName}...</Typography>
						</Box>
					)
					: (
						<Box className={classes.verticalRythm}>
							{content}
						</Box>
					)
				}
			</CardContent>

			<CardActions className={classes.buttons}>
				<Button
					className={classes.buttonSpace}
					startIcon={<DeleteIcon />}
					onClick={item && onDeleteItem(item.id)}
					disabled={!item}
				>
					Delete
				</Button>

				<Button
					className={classes.buttonSpace}
					startIcon={<RotateLeftIcon />}
					onClick={onResetItem}
					disabled={!item}
				>
					Reset
				</Button>

				<Button
					className={classes.buttonSpace}
					variant="contained"
					color="primary"
					startIcon={<SaveIcon />}
					onClick={onSaveItem}
					disabled={!item}
				>
					Save
				</Button>
			</CardActions>
		</Card>
	)
}

export default ItemDetails