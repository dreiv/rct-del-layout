import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({ spacing }) => ({
	buttons: {
		justifyContent: 'flex-end'
	},
	empty: {
		minHeight: '10rem'
	},
	verticalRythm: {
		'@global': {
			"> *:not(:last-child)": {
				marginBottom: spacing()
			}
		}
	}
}))
