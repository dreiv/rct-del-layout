import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({ spacing }) => ({
	buttons: {
		justifyContent: 'flex-end'
	},
	empty: {
		minHeight: '10rem'
	},
	listOffset: {
		marginLeft: -spacing(2),
		marginRight: -spacing(2),
		maxHeight: "20rem",
		overflow: "auto"
	}
}))
