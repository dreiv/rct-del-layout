import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import React from 'react';

import useStyles from './ItemList.styles';

const ItemList = ({
	itemName,
	items,
	selectedIndex,
	onAddItem,
	onSelectItem,
	onDeleteItem
}) => {
	const classes = useStyles()

	return (
		<Card>
			<CardHeader title={itemName + 's'} />
			<CardContent>
				{items.length === 0
					? (
						<Box
							display="flex"
							justifyContent="center"
							alignItems="center"
							className={classes.empty}
						>
							<Typography>No current {itemName}s...</Typography>
						</Box>
					)
					: (
						<List className={classes.listOffset}>
							{items.map((item, index) => (
								<ListItem
									key={index}
									button
									selected={selectedIndex === index}
									onClick={onSelectItem(index)}
								>
									<ListItemText
										primary={item.destination || `${itemName} (Draft)`}
										secondary={`Amount: ${item.amount}`}
									/>

									<ListItemSecondaryAction>
										<IconButton onClick={onDeleteItem(item.id)}>
											<DeleteIcon />
										</IconButton>
									</ListItemSecondaryAction>
								</ListItem>
							))}
						</List>
					)
				}
			</CardContent>

			<CardActions className={classes.buttons}>
				<Button
					variant="contained"
					color="primary"
					startIcon={<AddIcon />}
					onClick={onAddItem}
				>
					Add {itemName}
				</Button>
			</CardActions>
		</Card>
	)
}

export default ItemList