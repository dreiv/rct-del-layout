import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  FormControl,
  Input,
  InputLabel,
  Typography,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import SaveIcon from '@material-ui/icons/Save';
import React, { useEffect, useState } from 'react';

import useStyles from './ReceiptDetails.styles';

const ReceiptDetails = ({
	selectedReceipt,
	onDeleteReceipt,
	onSaveReceipt
}) => {
	const [receipt, setReceipt] = useState()
	const classes = useStyles()

	useEffect(() => {
		setReceipt(selectedReceipt)
	}, [selectedReceipt])

	const handleChange = prop => event => {
		setReceipt({
			...receipt,
			[prop]: event.target.value
		})
	}

	const handleCancel = () => {
		setReceipt(selectedReceipt)
	}

	return (
		<Card>
			<CardHeader title="Receipt Details" />

			<CardContent>
				{!receipt
					? (
						<Box
							display="flex"
							justifyContent="center"
							alignItems="center"
							className={classes.empty}
						>
							<Typography>Select or add a Receipt...</Typography>
						</Box>
					)
					: (
						<Box className={classes.verticalRythm}>
							<FormControl fullWidth >
								<InputLabel htmlFor="name">Name</InputLabel>
								<Input
									autoFocus
									id="name"
									value={receipt.name}
									onChange={handleChange('name')}
								/>
							</FormControl>

							<FormControl fullWidth >
								<InputLabel htmlFor="issuer">Issuer</InputLabel>
								<Input
									id="issuer"
									value={receipt.issuer}
									onChange={handleChange('issuer')}
								/>
							</FormControl>

							<FormControl fullWidth >
								<InputLabel htmlFor="amount">Amount</InputLabel>
								<Input
									id="amount"
									value={receipt.amount}
									onChange={handleChange('amount')}
								/>
							</FormControl>
						</Box>
					)
				}
			</CardContent>

			{receipt &&
				<CardActions className={classes.buttons}>
					<Button
						className={classes.buttonSpace}
						startIcon={<DeleteIcon />}
						onClick={onDeleteReceipt(receipt.id)}
					>
						Delete
					</Button>
					<Button
						className={classes.buttonSpace}
						startIcon={<RotateLeftIcon />}
						onClick={handleCancel}
					>
						Reset
					</Button>
					<Button
						className={classes.buttonSpace}
						variant="contained"
						color="primary"
						startIcon={<SaveIcon />}
						onClick={onSaveReceipt(receipt.id, receipt)}
					>
						Save
					</Button>
				</CardActions>
			}
		</Card>
	)
}

export default ReceiptDetails