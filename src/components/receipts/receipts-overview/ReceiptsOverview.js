import { Card, CardContent, CardHeader, Grid, Typography } from '@material-ui/core';
import React from 'react';

import useStyles from './ReceiptsOverview.styles';

const ReceiptsOverview = () => {
	const classes = useStyles()

	return (
		<Card>
			<CardHeader title={'Overview'} />

			<CardContent>
				<Grid container direction="column">
					<Grid item>
						<Grid container justify="space-between">
							<Grid item>
								<Typography>Low</Typography>
							</Grid>
							<Grid item>
								<Typography>123</Typography>
							</Grid>
						</Grid>
					</Grid>
					<Grid item>
						<Grid container justify="space-between">
							<Grid item>
								<Typography>High</Typography>
							</Grid>
							<Grid item>
								<Typography>456</Typography>
							</Grid>
						</Grid>
					</Grid>
					<Grid item>
						<Grid container justify="space-between">
							<Grid item>
								<Typography className={classes.bold}>Total</Typography>
							</Grid>
							<Grid item>
								<Typography className={classes.bold}>123456</Typography>
							</Grid>
						</Grid>
					</Grid>
				</Grid>
			</CardContent>
		</Card>
	)
}

export default ReceiptsOverview