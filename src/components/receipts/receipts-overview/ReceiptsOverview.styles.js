import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
	bold: {
		fontWeight: 'bold'
	}
}))
