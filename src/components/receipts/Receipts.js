import { Grid } from '@material-ui/core';
import ItemList from 'components/common/item-list';
import React, { useState } from 'react';

import ReceiptDetails from './receipt-details';
import ReceiptsOverview from './receipts-overview/ReceiptsOverview';

const Receipts = () => {
	const [receipts, setReceipts] = useState([])
	const [selectedReceipt, setSelectedReceipt] = useState()

	const onAddReceipt = () => {
		const newReceipt = {
			id: receipts.length,
			name: '',
			issuer: '',
			amount: 0
		}

		setReceipts([...receipts, newReceipt])
		setSelectedReceipt(newReceipt)
	}

	const onSaveReceipt = (receiptId, receipt) => () => {
		const receiptIdx = receipts.findIndex(({ id }) => id === receiptId)
		receipts[receiptIdx] = receipt

		setReceipts(receipts)
		setSelectedReceipt(null)
	}

	const onDeleteReceipt = deleteId => () => {
		const deleteIdx = receipts.findIndex(({ id }) => id === deleteId)
		receipts.splice(deleteIdx, 1)

		setReceipts(receipts)
		setSelectedReceipt(null)
	}

	const onSelectReceipt = receiptIdx => () => {
		setSelectedReceipt(receipts[receiptIdx])
	}

	return (
		<Grid container spacing={2}>
			<Grid item xs={12} sm={6}>
				<ItemList
					itemName="Receipt"
					items={receipts}
					onAddItem={onAddReceipt}
					onSelectItem={onSelectReceipt}
					onDeleteItem={onDeleteReceipt}
				/>
			</Grid>

			<Grid item xs={12} sm={6}>
				<ReceiptDetails
					selectedReceipt={selectedReceipt}
					onSaveReceipt={onSaveReceipt}
					onDeleteReceipt={onDeleteReceipt}
				/>
			</Grid>

			<Grid item xs={12} sm={6}>
				<ReceiptsOverview />
			</Grid>
		</Grid>

	)
}

export default Receipts