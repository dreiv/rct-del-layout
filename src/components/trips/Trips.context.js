import React, { createContext, useContext, useReducer } from 'react';

import { addTrip, changeDraftTrip, deleteTrip, resetTrip, saveTrip, selectTrip } from './actions';


const TripsStateContext = createContext()
const TripsDispatchContext = createContext()

function tripsReducer(state, action) {
	switch (action.type) {
		case actionTypes.addTrip: return addTrip(state)
		case actionTypes.selectTrip: return selectTrip(state, action.selectedIndex)
		case actionTypes.changeDraftTrip: return changeDraftTrip(state, action.property, action.value)

		case actionTypes.deleteTrip: return deleteTrip(state, action.deletedTripId)
		case actionTypes.saveTrip: return saveTrip(state)
		case actionTypes.resetTrip: return resetTrip(state)

		default: {
			throw new Error(`Unhandled action type: ${action.type}`)
		}
	}
}

const actionTypes = {
	addTrip: 'addTrip',
	selectTrip: 'selectTrip',
	changeDraftTrip: 'changeDraftTrip',
	deleteTrip: 'deleteTrip',
	saveTrip: 'saveTrip',
	resetTrip: 'resetTrip',
}

const initialState = {
	name: 'Trip',
	trips: []
}

function TripsProvider({ children }) {
	const [state, dispatch] = useReducer(tripsReducer, initialState)

	return (
		<TripsStateContext.Provider value={state}>
			<TripsDispatchContext.Provider value={dispatch}>
				{children}
			</TripsDispatchContext.Provider>
		</TripsStateContext.Provider>
	)
}

function useTripsState() {
	const context = useContext(TripsStateContext)

	if (context === undefined) {
		throw new Error('useTripsState must be used within a TripsProvider')
	}
	return context
}

function useTripsDispatch() {
	const context = useContext(TripsDispatchContext)

	if (context === undefined) {
		throw new Error('useTripsDispatch must be used within a TripsProvider')
	}
	return context
}

export {
	TripsProvider,
	useTripsState,
	useTripsDispatch,
	actionTypes
}