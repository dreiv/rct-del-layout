import { Grid, Typography } from '@material-ui/core';
import ItemDetails from 'components/common/item-details';
import ItemList from 'components/common/item-list';
import React from 'react';

import TripDetailsContent from './trip-details-content/TripDetailsContent';
import { actionTypes, TripsProvider, useTripsDispatch, useTripsState } from './Trips.context';
import useStyles from './Trips.styles';

const Trips = () => {
	const {
		name,
		trips,
		draftTrip,
		selectedIndex
	} = useTripsState()
	const dispatch = useTripsDispatch()
	const classes = useStyles()

	return (
		<>
			<Typography variant="h4">
				Business trips
			</Typography>

			<Grid container spacing={2} className={classes.container}>
				<Grid item xs={12} sm={6}>
					<ItemList
						itemName={name}
						items={trips}
						selectedIndex={selectedIndex}
						onAddItem={() => { dispatch({ type: actionTypes.addTrip }) }}
						onSelectItem={index => () => {
							dispatch({
								type: actionTypes.selectTrip,
								selectedIndex: index
							})
						}}
						onDeleteItem={id => () => {
							dispatch({
								type: actionTypes.deleteTrip,
								deletedTripId: id
							})
						}}
					/>
				</Grid>

				<Grid item xs={12} sm={6}>
					<ItemDetails
						itemName={name}
						item={draftTrip}
						onSaveItem={() => { dispatch({ type: actionTypes.saveTrip }) }}
						onDeleteItem={id => () => {
							dispatch({
								type: actionTypes.deleteTrip,
								deletedTripId: id
							})
						}}
						onResetItem={() => { dispatch({ type: actionTypes.resetTrip }) }}
						content={<TripDetailsContent />}
					/>
				</Grid>
			</Grid>
		</>
	)
}

export default () => (
	<TripsProvider>
		<Trips />
	</TripsProvider>
)