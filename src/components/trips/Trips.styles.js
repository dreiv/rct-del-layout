import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({ spacing }) => ({
	container: {
		marginTop: spacing(4)
	}
}))
