function saveTrip(state) {
	const { trips, selectedIndex, draftTrip } = state

	const newTrips = [...trips]
	newTrips[selectedIndex] = draftTrip

	return {
		...state,
		trips: newTrips
	}
}

export default saveTrip