function deleteTrip(state, deletedTripId) {
	const { trips } = state
	const deleteTripIdx = trips.findIndex(({ id }) => id === deletedTripId)

	const newTrips = [...trips]
	newTrips.splice(deleteTripIdx, 1)

	return {
		...state,
		trips: newTrips,
		draftTrip: null,
		selectedIndex: -1
	}
}

export default deleteTrip