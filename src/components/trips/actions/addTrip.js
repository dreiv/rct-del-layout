function addTrip(state) {
	const { trips } = state
	const newTrip = {
		id: Math.random().toString(36).substring(6),
		destination: '',
		from: '',
		to: '',
		amount: 0
	}

	return {
		...state,
		trips: [...trips, newTrip],
		draftTrip: newTrip,
		selectedIndex: trips.length
	}
}

export default addTrip