function selectTrip(state, selectedIndex) {
	const { trips } = state

	return {
		...state,
		draftTrip: trips[selectedIndex],
		selectedIndex
	}
}

export default selectTrip