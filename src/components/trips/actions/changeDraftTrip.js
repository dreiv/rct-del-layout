function changeDraftTrip(state, property, value) {
	const { draftTrip } = state

	return {
		...state,
		draftTrip: {
			...draftTrip,
			[property]: value
		}
	}
}

export default changeDraftTrip
