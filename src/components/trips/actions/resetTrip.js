function resetTrip(state) {
	const { trips, selectedIndex } = state

	return {
		...state,
		draftTrip: trips[selectedIndex]
	}
}

export default resetTrip