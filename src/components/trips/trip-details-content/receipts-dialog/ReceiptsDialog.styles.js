import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
	buttons: {
		justifyContent: 'flex-end'
	},
	card: {
		background: '#fafafa'
	}
}))
