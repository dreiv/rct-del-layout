import { Card, CardContent, CardHeader, Dialog, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Receipts from 'components/receipts';
import React from 'react';

import useStyles from './ReceiptsDialog.styles';

const ReceiptsDialog = ({
	dialogOpen,
	setDialogOpen
}) => {
	const classes = useStyles()

	return (
		<Dialog
			open={dialogOpen}
			onClose={() => { setDialogOpen(false) }}
			disableBackdropClick
		>
			<Card className={classes.card}>
				<CardHeader
					title='Business Trip Receipts'
					action={
						<IconButton onClick={() => { setDialogOpen(false) }}>
							<CloseIcon />
						</IconButton>
					}
				/>

				<CardContent>
					<Receipts />
				</CardContent>
			</Card>
		</Dialog>
	)
}

export default ReceiptsDialog