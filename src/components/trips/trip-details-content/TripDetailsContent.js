import { Button, FormControl, Input, InputLabel } from '@material-ui/core';
import React, { useState } from 'react';

import { actionTypes, useTripsDispatch, useTripsState } from '../Trips.context';
import ReceiptsDialog from './receipts-dialog/ReceiptsDialog';

const TripDetailsContent = () => {
	const [dialogOpen, setDialogOpen] = useState(false)

	const { draftTrip } = useTripsState()
	const dispatch = useTripsDispatch()

	const handleValueChange = property => ({ target: { value } }) => {
		dispatch({
			type: actionTypes.changeDraftTrip,
			property,
			value
		})
	}

	return (
		<>
			<ReceiptsDialog
				dialogOpen={dialogOpen}
				setDialogOpen={setDialogOpen}
			/>

			<FormControl fullWidth >
				<InputLabel htmlFor="destination">Destination</InputLabel>
				<Input
					autoFocus
					id="destination"
					value={draftTrip.destination}
					onChange={handleValueChange('destination')}
				/>
			</FormControl>

			<FormControl fullWidth >
				<InputLabel htmlFor="from">From(time)</InputLabel>
				<Input
					id="from"
					value={draftTrip.from}
					onChange={handleValueChange('from')}
				/>
			</FormControl>

			<FormControl fullWidth >
				<InputLabel htmlFor="to">To(time)</InputLabel>
				<Input
					id="to"
					value={draftTrip.to}
					onChange={handleValueChange('to')}
				/>
			</FormControl>

			<FormControl fullWidth >
				<Button onClick={() => { setDialogOpen(true) }}>Edit Receipts</Button>
			</FormControl>
		</>
	)
}

export default TripDetailsContent