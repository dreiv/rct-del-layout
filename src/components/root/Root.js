import { Container, CssBaseline } from '@material-ui/core';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Trips from 'components/trips';
import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';

import useStyles from './Root.styles';

const theme = createMuiTheme({})

const Root = () => {
	const classes = useStyles()

	return (
		<ThemeProvider theme={theme}>
			<CssBaseline />

			<Container className={classes.content}>
				<Suspense fallback="Loading...">
					<Switch>
						<Route exact path="/" component={Trips} />
					</Switch>
				</Suspense>
			</Container>
		</ThemeProvider>
	)
}

export default Root