import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({ spacing }) => ({
	content: {
		marginTop: spacing(4),
		marginBottom: spacing(4)
	}
}))
