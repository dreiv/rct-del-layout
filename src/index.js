import './index.css';

import Root from 'components/root';
import { createBrowserHistory } from 'history';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';

import * as serviceWorker from './serviceWorker';

const isProd = process.env.NODE_ENV === 'production'
const history = createBrowserHistory({
	...isProd && { basename: 'rct-del-layout' }
})

ReactDOM.render(
	<Router history={history}>
		<Root />
	</Router>
	, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
